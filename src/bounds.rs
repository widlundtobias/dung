#![allow(dead_code)]

use approx::AbsDiffEq;
use num::{Bounded, FromPrimitive};
use simba::scalar::{ClosedAdd, ClosedMul, ClosedSub};

pub trait BoundsNumber:
    glm::Scalar
    + Copy
    + PartialOrd
    + ClosedAdd
    + ClosedSub
    + ClosedMul
    + AbsDiffEq<Epsilon = Self>
    + FromPrimitive
    + Bounded
{
    /// Convert the extent into one that has got a positive size
    ///
    /// A an extent that is negative can be created if
    /// the size is negative. This function will effectively
    /// make sure that if the size is negative, then
    /// it will be flipped around to positive with the start point
    /// adjusted so that the absolute extent is the same
    fn turn_positive(start: Self, size: Self) -> (Self, Self);
}

impl BoundsNumber for f32 {
    fn turn_positive(start: Self, size: Self) -> (Self, Self) {
        let (start, size) = if size < FromPrimitive::from_f32(0.0).unwrap() {
            (start + size, -size)
        } else {
            (start, size)
        };

        (start, size)
    }
}
impl BoundsNumber for u32 {
    fn turn_positive(start: Self, size: Self) -> (Self, Self) {
        (start, size)
    }
}

/// Represents an enclosing rectangle in world-space
///
/// Start is the top-left corner
/// End is the bottom-right corner.
/// Size is the extent between these.
///
/// Size is always positive
#[derive(Debug, Copy, Clone)]
pub struct Bounds3<T: BoundsNumber> {
    pub start: glm::TVec3<T>,
    pub size: glm::TVec3<T>,
}

pub enum PointOrBounds3<T: BoundsNumber> {
    Point(glm::TVec3<T>),
    Bounds(Bounds3<T>),
}

#[derive(Debug, Copy, Clone)]
pub struct Corner3<T> {
    pub orientation: Corner3Orientation,
    pub position: glm::TVec3<T>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Corner3Orientation {
    BackBottomRight,
    BackBottomLeft,
    BackTopLeft,
    BackTopRight,
    FrontBottomRight,
    FrontBottomLeft,
    FrontTopLeft,
    FrontTopRight,
}
impl Corner3Orientation {
    pub fn opposite(&self) -> Corner3Orientation {
        match self {
            Corner3Orientation::BackBottomRight => Corner3Orientation::FrontTopLeft,
            Corner3Orientation::BackBottomLeft => Corner3Orientation::FrontTopRight,
            Corner3Orientation::BackTopLeft => Corner3Orientation::FrontBottomRight,
            Corner3Orientation::BackTopRight => Corner3Orientation::FrontBottomLeft,
            Corner3Orientation::FrontBottomRight => Corner3Orientation::BackTopLeft,
            Corner3Orientation::FrontBottomLeft => Corner3Orientation::BackTopRight,
            Corner3Orientation::FrontTopLeft => Corner3Orientation::BackBottomRight,
            Corner3Orientation::FrontTopRight => Corner3Orientation::BackBottomLeft,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EdgeOrientation {
    Left,
    Right,
    Top,
    Bottom,
}

impl EdgeOrientation {
    pub fn is_vertical(&self) -> bool {
        match self {
            EdgeOrientation::Left | EdgeOrientation::Right => true,
            EdgeOrientation::Top | EdgeOrientation::Bottom => false,
        }
    }
    pub fn opposite(&self) -> EdgeOrientation {
        match self {
            EdgeOrientation::Left => EdgeOrientation::Right,
            EdgeOrientation::Right => EdgeOrientation::Left,
            EdgeOrientation::Top => EdgeOrientation::Bottom,
            EdgeOrientation::Bottom => EdgeOrientation::Top,
        }
    }
}

impl<T: BoundsNumber> Bounds3<T> {
    /// Create a new Bounds3 object with a zeroed out start/size
    pub fn zero() -> Self {
        let zero = FromPrimitive::from_f32(0.0).unwrap();
        let zero = glm::vec3(zero, zero, zero);
        Self {
            start: zero,
            size: zero,
        }
    }
    /// Create a new Bounds3 object with the given top-left and bottom-right corner positions
    /// If start/end would make a negative size, then they will be flipped accordingly.
    /// The resulting bounds will cover the same area.
    pub fn with_start_end(start: glm::TVec3<T>, end: glm::TVec3<T>) -> Self {
        let size = end - start;
        let (start, size) = Self::turn_vec_positive(start, size);
        Self { start, size }
    }
    /// Create a new Bounds3 object with the given top-left and size
    /// If size is negative, start/size will be adjusted to make size positive.
    /// The resulting bounds will cover the same area.
    pub fn with_start_size(start: glm::TVec3<T>, size: glm::TVec3<T>) -> Self {
        Self::with_start_end(start, start + size)
    }
    /// Access the bottom-right corner of the Bounds3
    pub fn end(&self) -> glm::TVec3<T> {
        self.start + self.size
    }
    /// Access the size of the Bounds3. Always positive
    pub fn size(&self) -> glm::TVec3<T> {
        self.size
    }
    pub fn set_size(&mut self, size: glm::TVec3<T>) {
        let start = self.start;
        *self = Self::with_start_size(start, size);
    }
    /// Check if two Bounds3 are overlapping
    pub fn is_overlapping(&self, other: &Bounds3<T>) -> bool {
        let a = self;
        let b = other;

        let a_end = a.end();
        let b_end = b.end();

        let not_overlapping = a.start.x > b_end.x
            || b.start.x > a_end.x
            || a.start.y > b_end.y
            || b.start.y > a_end.y;

        !not_overlapping
    }
    /// Check if a point is contained by the Bounds3 object
    pub fn contains(&self, pos: &glm::TVec3<T>) -> bool {
        let start = self.start;
        let end = self.end();

        pos.x > start.x
            && pos.x < end.x
            && pos.y > start.y
            && pos.y < end.y
            && pos.z > start.z
            && pos.z < end.z
    }
    /// Create a bounds object that encompasses both
    pub fn extended_by(&self, other: &Bounds3<T>) -> Bounds3<T> {
        let min = |a, b| {
            if a < b {
                a
            } else {
                b
            }
        };
        let max = |a, b| {
            if a > b {
                a
            } else {
                b
            }
        };
        let start = glm::vec3(
            min(self.start.x, other.start.x),
            min(self.start.y, other.start.y),
            min(self.start.z, other.start.z),
        );
        let end = glm::vec3(
            max(self.end().x, other.end().x),
            max(self.end().y, other.end().y),
            max(self.end().z, other.end().z),
        );
        Bounds3::with_start_end(start, end)
    }

    /// Retrieve the corners of the bounds
    pub fn corners(&self) -> [Corner3<T>; 8] {
        let bbl = self.end();
        let ftr = self.start;

        let bbr = glm::vec3(bbl.x, bbl.y, ftr.z);
        let btl = glm::vec3(bbl.x, ftr.y, bbl.z);
        let btr = glm::vec3(bbl.x, ftr.y, ftr.z);
        let fbr = glm::vec3(ftr.x, bbl.y, ftr.z);
        let fbl = glm::vec3(ftr.x, bbl.y, bbl.z);
        let ftl = glm::vec3(ftr.x, ftr.y, bbl.z);

        [
            Corner3 {
                orientation: Corner3Orientation::BackBottomRight,
                position: bbr,
            },
            Corner3 {
                orientation: Corner3Orientation::BackBottomLeft,
                position: bbl,
            },
            Corner3 {
                orientation: Corner3Orientation::BackTopLeft,
                position: btl,
            },
            Corner3 {
                orientation: Corner3Orientation::BackTopRight,
                position: btr,
            },
            Corner3 {
                orientation: Corner3Orientation::FrontBottomRight,
                position: fbr,
            },
            Corner3 {
                orientation: Corner3Orientation::FrontBottomLeft,
                position: fbl,
            },
            Corner3 {
                orientation: Corner3Orientation::FrontTopLeft,
                position: ftl,
            },
            Corner3 {
                orientation: Corner3Orientation::FrontTopRight,
                position: ftr,
            },
        ]
    }

    /// Retrieve the opposite corner
    pub fn opposite_corner(&self, orientation: Corner3Orientation) -> Corner3<T> {
        let opposite_orientation = orientation.opposite();

        let corners = self.corners();
        *corners
            .iter()
            .find(|corner| corner.orientation == opposite_orientation)
            .unwrap() // Unwrap is OK since every bounds object has every corner always
    }

    fn turn_vec_positive(
        start: glm::TVec3<T>,
        size: glm::TVec3<T>,
    ) -> (glm::TVec3<T>, glm::TVec3<T>) {
        let (x_start, x_size) = BoundsNumber::turn_positive(start.x, size.x);
        let (y_start, y_size) = BoundsNumber::turn_positive(start.y, size.y);
        let (z_start, z_size) = BoundsNumber::turn_positive(start.z, size.z);
        (
            glm::vec3(x_start, y_start, z_start),
            glm::vec3(x_size, y_size, z_size),
        )
    }
}

impl<T: glm::RealField + BoundsNumber> Bounds3<T> {
    /// Create a new Bounds3 object with the given center point and size.
    /// If size is negative, the bounds will be adjusted to cover the same area
    /// but with a positive size.
    pub fn with_center(center: glm::TVec3<T>, size: glm::TVec3<T>) -> Self {
        let half_size = size / T::from_f32(2.0).unwrap();
        let start = center - half_size;
        let (start, size) = Self::turn_vec_positive(start, size);
        Self { start, size }
    }

    /// Calculates the center of the bounds
    pub fn center(&self) -> glm::TVec3<T> {
        self.start + self.size / T::from_f32(2.0).unwrap()
    }
    /// A selction object will either describe a location as a point or bounds depending on how close the start is from end
    pub fn as_point_if_tiny(&self, tiny_threshold: T) -> PointOrBounds3<T> {
        // If our box is tiny, it acts as a point
        let is_tiny = self.size().x < tiny_threshold
            && self.size().y < tiny_threshold
            && self.size().z < tiny_threshold;

        if is_tiny {
            PointOrBounds3::Point(self.center())
        } else {
            PointOrBounds3::Bounds(*self)
        }
    }
}
