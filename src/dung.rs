use bevy::prelude::*;
use bevy_fly_camera::{FlyCamera, FlyCameraPlugin};
use level::{LevelTilesAsset, LevelTilesAssetLoader};
use voxels::ZoneCoord;
extern crate nalgebra_glm as glm;
extern crate special_utils as util;

mod bounds;
mod level;
mod settings;
mod voxel_ecs;
mod voxels;

fn main() {
    App::new()
        .add_system(capture_mouse_system)
        .add_system(bevy::input::system::exit_on_esc_system)
        .add_system(toggle_capture_mouse_system)
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(settings::Settings::default())
        .add_plugins(DefaultPlugins) // Default set of plugins from the Bevy engine
        .add_plugin(DungPlugin) // Custom plugin defined in this file
        .add_plugin(bevy_inspector_egui::WorldInspectorPlugin::new())
        .run();
}

fn capture_mouse_system(mut windows: ResMut<Windows>, settings: Res<settings::Settings>) {
    let window = windows.get_primary_mut().unwrap();

    let is_mouse_captured = window.cursor_locked();

    if is_mouse_captured != settings.capture_mouse {
        window.set_cursor_lock_mode(settings.capture_mouse);
        window.set_cursor_visibility(!settings.capture_mouse);
    }
}

fn toggle_capture_mouse_system(
    mut settings: ResMut<settings::Settings>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::M) {
        info!("Toggling mouse capture");
        settings.capture_mouse = !settings.capture_mouse;
    }
}

fn load_scene(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut connect_zones_events: EventWriter<voxel_ecs::entrance::ConnectZonesEvent>,
    asset_server: Res<AssetServer>,
) {
    // camera
    commands
        .spawn_bundle(PerspectiveCameraBundle {
            transform: Transform::from_xyz(50.0, 50.5, 50.0)
                .looking_at(Vec3::new(16., 16., 16.), Vec3::Y),
            ..Default::default()
        })
        .insert(FlyCamera::default())
        //.insert(PointLight {
        //    range: 200.,
        //    ..Default::default()
        //})
        .insert(voxel_ecs::entrance::EntranceTransportable::new());

    // Sky
    commands.insert_resource(ClearColor(Color::rgb_u8(135, 206, 235)));

    // Ambient light
    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 0.05,
    });

    // Sun
    commands.spawn_bundle(DirectionalLightBundle {
        directional_light: DirectionalLight {
            // Configure the projection to better fit the scene
            //shadow_projection: OrthographicProjection {
            //    left: -HALF_SIZE,
            //    right: HALF_SIZE,
            //    bottom: -HALF_SIZE,
            //    top: HALF_SIZE,
            //    near: -10.0 * HALF_SIZE,
            //    far: 10.0 * HALF_SIZE,
            //    ..Default::default()
            //},
            shadows_enabled: true,
            illuminance: 10000.0,
            ..Default::default()
        },
        transform: Transform {
            translation: Vec3::new(0.0, 200.0, 0.0),
            rotation: Quat::from_rotation_x(-std::f32::consts::FRAC_PI_4),
            ..Default::default()
        },
        ..Default::default()
    });

    /*
    // spawn a voxel zone
    let zone_1 = voxel_ecs::VoxelZoneBuilder::new(
        glm::zero(),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(20, 12, 31)),
    )
    .add_entrance(0, ZoneCoord::new(19, 1, 15))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    let zone_2 = voxel_ecs::VoxelZoneBuilder::new(
        glm::vec3(0., 20., 0.),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(34, 6, 12)),
    )
    .add_entrance(0, ZoneCoord::new(0, 1, 5))
    .add_entrance(1, ZoneCoord::new(10, 1, 11))
    .add_entrance(2, ZoneCoord::new(19, 1, 11))
    .add_entrance(3, ZoneCoord::new(30, 1, 11))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    let zone_3 = voxel_ecs::VoxelZoneBuilder::new(
        glm::vec3(0., 30., 0.),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(9, 7, 19)),
    )
    .add_entrance(0, ZoneCoord::new(4, 1, 0))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    let zone_4 = voxel_ecs::VoxelZoneBuilder::new(
        glm::vec3(0., 40., 0.),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(4, 4, 32)),
    )
    .add_entrance(0, ZoneCoord::new(2, 1, 0))
    .add_entrance(1, ZoneCoord::new(2, 1, 31))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    let zone_5 = voxel_ecs::VoxelZoneBuilder::new(
        glm::vec3(0., 50., 0.),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(30, 8, 18)),
    )
    .add_entrance(0, ZoneCoord::new(4, 1, 0))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    let zone_6 = voxel_ecs::VoxelZoneBuilder::new(
        glm::vec3(0., 60., 0.),
        voxel_ecs::VoxelZoneSource::SizedBox(ZoneCoord::new(83, 40, 47)),
    )
    .add_entrance(0, ZoneCoord::new(40, 1, 0))
    .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

    connect_zones_events.send(voxel_ecs::entrance::ConnectZonesEvent::new(
        zone_1, 0, zone_2, 0,
    ));
    connect_zones_events.send(voxel_ecs::entrance::ConnectZonesEvent::new(
        zone_2, 1, zone_3, 0,
    ));
    connect_zones_events.send(voxel_ecs::entrance::ConnectZonesEvent::new(
        zone_2, 2, zone_4, 0,
    ));
    connect_zones_events.send(voxel_ecs::entrance::ConnectZonesEvent::new(
        zone_2, 3, zone_5, 0,
    ));
    connect_zones_events.send(voxel_ecs::entrance::ConnectZonesEvent::new(
        zone_4, 1, zone_6, 0,
    ));*/

    //let handle: Handle<LevelTilesAsset> = asset_server.load("maps/green.pngl");
    //Box::leak(Box::new(handle)); // TODO: lmao
    //let handle: Handle<LevelTilesAsset> = asset_server.load("maps/sand.pngl");
    //Box::leak(Box::new(handle)); // TODO: lmao
    let handle: Handle<LevelTilesAsset> = asset_server.load("maps/haylands.pngl");
    Box::leak(Box::new(handle)); // TODO: lmao
}

pub struct DungPlugin;

impl Plugin for DungPlugin {
    fn build(&self, app: &mut App) {
        // Setup our custom Bevy plugin by adding systems/resources to it

        app.add_plugin(FlyCameraPlugin)
            .add_asset::<LevelTilesAsset>()
            .init_asset_loader::<LevelTilesAssetLoader>()
            .add_event::<voxel_ecs::entrance::ConnectZonesEvent>()
            .add_startup_system(load_scene)
            .add_system(voxel_ecs::entrance::connect_entrances)
            .add_system(voxel_ecs::zone::update_zone_spans)
            .add_system(voxel_ecs::entrance::entrance_transport)
            .add_system(spawn_loaded_levels)
            .insert_resource(bevy::pbr::AmbientLight {
                color: Color::WHITE,
                brightness: 1.0 / 5.0f32,
            });
    }
}

fn spawn_loaded_levels(
    mut next_height: Local<f32>,
    mut ev_asset: EventReader<AssetEvent<LevelTilesAsset>>,
    assets: ResMut<Assets<LevelTilesAsset>>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    for ev in ev_asset.iter() {
        if let AssetEvent::Created { handle } = ev {
            let asset = assets.get(handle).unwrap();
            println!("Level '{}' loaded!", &asset.name);

            voxel_ecs::VoxelZoneBuilder::new(
                glm::vec3(0., *next_height, 0.),
                voxel_ecs::VoxelZoneSource::Level(asset),
            )
            .add_entrance(0, ZoneCoord::new(0, 1, 5))
            .add_entrance(1, ZoneCoord::new(10, 1, 11))
            .add_entrance(2, ZoneCoord::new(19, 1, 11))
            .add_entrance(3, ZoneCoord::new(30, 1, 11))
            .spawn(&mut commands, &*asset_server, &mut *meshes, &mut *materials);

            *next_height += 80.0;
        }
    }
}
