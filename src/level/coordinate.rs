/// Tile coord inside a level
#[derive(Debug, Clone, Copy)]
pub struct LevelCoord(pub glm::UVec2);

impl LevelCoord {
    pub fn new(x: u32, y: u32) -> Self {
        Self(glm::vec2(x, y))
    }
}
