mod place_cactus;
pub use place_cactus::*;

mod rock_wall;
pub use rock_wall::*;

use std::fmt::Debug;

use rand::prelude::ThreadRng;

use crate::voxel_ecs::zone::ZoneBounds;

use super::LevelVoxels;

pub trait LevelGenEvent: Debug {
    fn extent(&self, level: &LevelVoxels) -> ZoneBounds;
    fn resolve(&self, level: &mut LevelVoxels, rng: &mut ThreadRng);
}
