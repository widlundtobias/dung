use rand::{prelude::ThreadRng, Rng};

use crate::{
    level::{coordinate::LevelCoord, LevelVoxels},
    voxel_ecs::zone,
    voxels::{self, ZoneCoord},
};

use super::LevelGenEvent;

#[derive(Debug)]
pub struct PlaceCactusEvent {
    pub coord: LevelCoord,
}

impl LevelGenEvent for PlaceCactusEvent {
    fn extent(&self, _: &LevelVoxels) -> zone::ZoneBounds {
        zone::ZoneBounds::new(ZoneCoord::new(0, 0, 0), ZoneCoord::new(3, 3, 3))
    }

    fn resolve(&self, level: &mut LevelVoxels, rng: &mut ThreadRng) {
        let cactus_height = rng.gen_range(1..=4);
        for y in 0..cactus_height {
            let target_coord = glm::vec3(self.coord.0.x, y + 1, self.coord.0.y);
            level.set(&target_coord, Some(voxels::Type::Cactus))
        }
    }
}
