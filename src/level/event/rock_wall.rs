use rand::prelude::ThreadRng;

use crate::{
    level::{coordinate::LevelCoord, LevelVoxels},
    voxel_ecs::zone,
    voxels::{self, ZoneCoord},
};

use super::LevelGenEvent;

#[derive(Debug)]
pub struct RockWallEvent {
    pub coord: LevelCoord,
}

impl LevelGenEvent for RockWallEvent {
    fn extent(&self, level: &LevelVoxels) -> zone::ZoneBounds {
        zone::ZoneBounds::new(
            ZoneCoord::new(0, 0, 0),
            ZoneCoord::new(1, level.size.0.y, 1),
        )
    }

    fn resolve(&self, level: &mut LevelVoxels, _rng: &mut ThreadRng) {
        for y in 0..(level.size.0.y - 1) {
            let target_coord = glm::vec3(self.coord.0.x, y + 1, self.coord.0.y);
            level.set(&target_coord, Some(voxels::Type::Granite))
        }
    }
}
