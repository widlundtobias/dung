use std::{convert::TryInto, io::Cursor};

use bevy::{
    asset::{AssetLoader, LoadContext, LoadedAsset},
    reflect::TypeUuid,
    utils::BoxedFuture,
};

use image::io::Reader as ImageReader;

use anyhow::anyhow;
use serde::Deserialize;

use crate::voxels;

#[derive(Debug, Clone, Copy, Deserialize)]
pub enum TileType {
    Grass,
    Forest,
    HillsLow,
    HillsHigh,
    DryGrass,
    Sand,
    River,
    Bridge,
    Road,
    Building,
    CactusField,
    Mountain,
    RockWall,
}

impl From<TileType> for voxels::Type {
    fn from(val: TileType) -> Self {
        match val {
            TileType::Grass => voxels::Type::Grass,
            TileType::Forest => voxels::Type::Moss,
            TileType::Road => voxels::Type::Road,
            TileType::HillsLow => voxels::Type::Moss,
            TileType::HillsHigh => voxels::Type::Moss,
            TileType::Sand => voxels::Type::Sand,
            TileType::CactusField => voxels::Type::Sand,
            TileType::Building => voxels::Type::Planks,
            TileType::Bridge => voxels::Type::Planks,
            TileType::Mountain => voxels::Type::Granite,
            TileType::DryGrass => voxels::Type::DryGrass,
            TileType::River => voxels::Type::Water,
            TileType::RockWall => voxels::Type::Granite,
            #[allow(unreachable_patterns)]
            tile => {
                println!(
                    "No voxel to match for level tile {:?}, falling back to grass",
                    tile
                );
                voxels::Type::Grass
            }
        }
    }
}

impl std::convert::TryInto<TileType> for [u8; 4] {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<TileType, Self::Error> {
        match self {
            [197, 180, 94, _] => Ok(TileType::Sand),
            [126, 151, 62, _] => Ok(TileType::DryGrass),
            [91, 134, 178, _] => Ok(TileType::River),
            [80, 135, 62, _] => Ok(TileType::Grass),
            [110, 110, 110, _] => Ok(TileType::Bridge),
            [27, 54, 27, _] => Ok(TileType::Forest),
            [10, 36, 10, _] => Ok(TileType::HillsLow),
            [1, 24, 1, _] => Ok(TileType::HillsHigh),
            [53, 37, 14, _] => Ok(TileType::Building),
            [171, 188, 110, _] => Ok(TileType::CactusField),
            [89, 89, 89, _] => Ok(TileType::Mountain),
            [161, 123, 67, _] => Ok(TileType::Road),
            [104, 42, 6, _] => Ok(TileType::RockWall),
            _ => {
                println!("unsupported color {:?}", self);
                Err(anyhow!("unsupported color {:?}", self))
            }
        }
    }
}

#[derive(Debug, Deserialize, TypeUuid)]
#[uuid = "fb2e9ed1-ae7a-4010-9c5a-8eff5917ec2b"]
pub struct LevelTilesAsset {
    pub name: String,
    pub size: glm::UVec2,
    tiles: Vec<TileType>,
}

impl LevelTilesAsset {
    pub fn new(name: String, size: glm::UVec2) -> Self {
        Self {
            name,
            size,
            tiles: vec![TileType::Grass; (size.x * size.y) as usize],
        }
    }
    pub fn set(&mut self, pos: &glm::UVec2, tile: TileType) {
        let index = (pos.x + pos.y * self.size.x) as usize;
        self.tiles[index] = tile;
    }
    pub fn get(&self, pos: &glm::UVec2) -> Option<TileType> {
        let index = (pos.x + pos.y * self.size.x) as usize;
        self.tiles.get(index).cloned()
    }
}

#[derive(Default)]
pub struct LevelTilesAssetLoader;

impl AssetLoader for LevelTilesAssetLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            println!("gonna load level");
            let image =
                ImageReader::with_format(Cursor::new(bytes), image::ImageFormat::Png).decode()?;
            let pixels = image.to_rgba8();
            let width = pixels.width();
            let height = pixels.height();

            println!("gonna load pixels in image");

            let name = load_context
                .path()
                .file_stem()
                .ok_or_else(|| {
                    anyhow!(
                        "Could not get level name of {:?}",
                        load_context.path().to_str()
                    )
                })?
                .to_str()
                .map(|str| str.to_owned())
                .unwrap_or_default();

            let mut level = LevelTilesAsset::new(name, glm::vec2(width, height));

            for y in 0..height {
                for x in 0..width {
                    let tile: TileType = pixels.get_pixel(x, y).0.try_into()?;
                    level.set(&glm::vec2(x, y), tile);
                }
            }

            load_context.set_default_asset(LoadedAsset::new(level));

            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["pngl"]
    }
}
