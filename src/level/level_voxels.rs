use crate::voxels;

use super::{coordinate::LevelCoord, event, LevelTilesAsset, TileType};

pub struct LevelVoxels {
    pub size: voxels::ZoneCoord,
    pub voxels: Vec<Option<voxels::Type>>,
}

impl LevelVoxels {
    pub fn from_tiles(tiles: &LevelTilesAsset) -> Self {
        println!("from tiles");
        let height = 10;
        let size = voxels::ZoneCoord::new(tiles.size.x, height, tiles.size.y);
        let voxel_count = (size.0.x * size.0.y * size.0.z) as _;
        let voxels = vec![None; voxel_count];

        let mut rng = rand::thread_rng();

        let mut level = Self { size, voxels };

        let mut events = Vec::new();

        for z in 0..size.0.z {
            for x in 0..size.0.x {
                let coord = LevelCoord::new(x, z);
                // What tile do we have at this coordinate?
                let tile = tiles.get(&glm::vec2(x, z));

                // Use this tile to maybe create a level gen event
                if let Some(tile) = tile {
                    if let Some(event) = random_event_from_tile(coord, tile, &mut rng) {
                        events.push(event);
                    }
                }

                // What base floor voxel does the tile give?
                let floor_voxel = tile.map(|t| t.into());
                level.set(&glm::vec3(x, 0, z), floor_voxel);
            }
        }

        println!("================events: {:?}", events.len());

        for event in events {
            event.resolve(&mut level, &mut rng);
        }

        level
    }
    pub fn from_size(size: glm::UVec3) -> Self {
        println!("from size");

        let voxel_count = (size.x * size.y * size.z) as _;
        let voxels = vec![Some(voxels::Type::Grass); voxel_count];

        Self {
            size: voxels::ZoneCoord(size),
            voxels,
        }
    }
    pub fn get(&self, pos: &glm::UVec3) -> Option<voxels::Type> {
        let i = pos.x + pos.y * self.size.0.x + pos.z * self.size.0.x * self.size.0.y;
        self.voxels[i as usize]
    }
    pub fn set(&mut self, pos: &glm::UVec3, voxel: Option<voxels::Type>) {
        let i = pos.x + pos.y * self.size.0.x + pos.z * self.size.0.x * self.size.0.y;
        self.voxels[i as usize] = voxel;
    }
}

fn random_event_from_tile(
    coord: LevelCoord,
    tile: TileType,
    rng: &mut impl rand::Rng,
) -> Option<Box<dyn event::LevelGenEvent>> {
    const CACTUS_CHANCE: f64 = 1. / 16.;

    match tile {
        TileType::CactusField if rng.gen_bool(CACTUS_CHANCE) => {
            Some(Box::new(event::PlaceCactusEvent { coord }))
        }
        TileType::RockWall => Some(Box::new(event::RockWallEvent { coord })),
        _ => None,
    }
}
