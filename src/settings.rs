pub struct Settings {
    pub capture_mouse: bool,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            capture_mouse: true,
        }
    }
}
