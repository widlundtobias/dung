extern crate nalgebra_glm as glm;

use bevy::prelude::*;

// This only contains very few special helper functions that should be globally available without including anything in dung

/// Pretty debug friendly to-text function for Vec
pub fn pp(glm: glm::Vec3) -> String {
    format!("[{},{},{}]", glm.x, glm.y, glm.z)
}

/// convert between bevy/glm
pub fn to_glm(glam: Vec3) -> glm::Vec3 {
    glm::vec3(glam.x, glam.y, glam.z)
}

/// convert between bevy/glm
pub fn from_glm(glm: glm::Vec3) -> Vec3 {
    bevy::math::vec3(glm.x, glm.y, glm.z)
}
