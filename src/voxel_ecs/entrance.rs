use bevy::prelude::*;

use crate::voxels;

use super::zone;

pub struct ConnectZonesEvent {
    pub a: voxels::ZoneEntranceRef,
    pub b: voxels::ZoneEntranceRef,
}

/// A component that makes an entity possible to transport in entrances
#[derive(Component)]
pub struct EntranceTransportable {
    pub ready: bool,
}

impl EntranceTransportable {
    pub fn new() -> Self {
        Self { ready: true }
    }
}

impl ConnectZonesEvent {
    pub fn new(zone_a: Entity, entrance_a: u32, zone_b: Entity, entrance_b: u32) -> Self {
        Self {
            a: voxels::ZoneEntranceRef {
                zone: zone_a,
                entrance_id: entrance_a,
            },
            b: voxels::ZoneEntranceRef {
                zone: zone_b,
                entrance_id: entrance_b,
            },
        }
    }
}

/// This system performs entrance connections based on events
pub fn connect_entrances(
    mut connect_zones_events: EventReader<ConnectZonesEvent>,
    mut q: Query<&mut zone::ZoneEntrances>,
) {
    for connect_zones in connect_zones_events.iter() {
        let a = connect_zones.a;
        let b = connect_zones.b;

        let a_exists = q
            .get_mut(a.zone)
            .ok()
            .map(|e| e.entrances.iter().any(|e| e.id == a.entrance_id))
            == Some(true);
        let b_exists = q
            .get_mut(b.zone)
            .ok()
            .map(|e| e.entrances.iter().any(|e| e.id == b.entrance_id))
            == Some(true);

        if a_exists && b_exists {
            q.get_mut(a.zone)
                .unwrap()
                .entrances
                .iter_mut()
                .find(|e| e.id == a.entrance_id)
                .unwrap()
                .target = Some(b);
            q.get_mut(b.zone)
                .unwrap()
                .entrances
                .iter_mut()
                .find(|e| e.id == b.entrance_id)
                .unwrap()
                .target = Some(a);

            println!(
                "Successfully connected zone entrances {:?}:{} and {:?}:{}",
                a.zone, a.entrance_id, b.zone, b.entrance_id,
            );
        } else {
            println!(
                "Failed to connect zone entrances {:?}:{} and {:?}:{} because {} does not exist",
                a.zone,
                a.entrance_id,
                b.zone,
                b.entrance_id,
                match (a_exists, b_exists) {
                    (true, false) => "the second",
                    (false, true) => "the first",
                    (false, false) => "both",
                    _ => "<bug, both did exist>",
                }
            )
        }
    }
}

/// This system transports transportable entities between connected entrances
pub fn entrance_transport(
    zone_q: Query<(Entity, &zone::ZoneEntrances, &zone::ZoneWorldBounds)>,
    mut transportables_q: Query<(
        Entity,
        &mut EntranceTransportable,
        &mut Transform, //TODO: if this item is a child, then this transform won't be global coords but those should not be transportable?
    )>,
) {
    for (entity, mut transportable, mut local_trans) in transportables_q.iter_mut() {
        let mut is_in_entrance = false;

        // Find the entrances of the zone it's in, if any
        if let Some((source_zone, entrances, source_zone_bounds)) = zone::find_containing_zone(
            zone_q.iter().map(|(entity, _, bounds)| (entity, bounds)),
            &util::to_glm(local_trans.translation),
        )
        .and_then(|z| zone_q.get(z).ok())
        {
            // Inside a zone, check if I'm close enough to any of the entrances
            let inside_entrance = entrances.entrances.iter().find(|e| {
                let world_bounds = e.world_bounds(&source_zone_bounds.0.start);
                world_bounds.contains(&util::to_glm(local_trans.translation))
            });

            if let Some(source_entrance) = inside_entrance {
                is_in_entrance = true;

                if transportable.ready {
                    if let Some(target) = source_entrance.target {
                        let target_entrance_id = target.entrance_id;

                        if let Some((target_zone, target_entrance, target_zone_bounds)) = zone_q
                            .get(target.zone)
                            .ok()
                            .and_then(|(zone, entrances, bounds)| {
                                entrances
                                    .find_by_id(target_entrance_id)
                                    .map(|entrance| (zone, entrance, bounds))
                            })
                        {
                            //// Teleport to target zone's entrance.
                            // Find the delta pos between the source and target
                            let source_start = source_entrance
                                .world_bounds(&source_zone_bounds.0.start)
                                .start;
                            let target_start = target_entrance
                                .world_bounds(&target_zone_bounds.0.start)
                                .start;

                            let teleport_delta = target_start - source_start;
                            println!("Entity {:?} is inside of entrance and will teleport from zone:entrance {:?}:{:?}->{:?}:{:?} or pos {}->{} (d:{})",entity, source_zone, source_entrance.id, target_zone, target_entrance.id, util::pp(source_start), util::pp(target_start), util::pp(teleport_delta));
                            local_trans.translation += util::from_glm(teleport_delta);
                            // Set ready to false
                            transportable.ready = false;
                        }
                    }
                }
            }
        }

        // If it wasn't close to an entrance, set ready to true
        if !is_in_entrance {
            transportable.ready = true;
        }
    }
}
