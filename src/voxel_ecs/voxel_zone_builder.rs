use bevy::prelude::*;
use rand::{prelude::SliceRandom, Rng};

use super::*;

use crate::{
    bounds,
    level::{LevelTilesAsset, LevelVoxels},
    voxels::{self, ChunkCoord, WorldCoord, ZoneCoord, ZoneEntrance},
};

pub enum VoxelZoneSource<'a> {
    SizedBox(ZoneCoord),
    Level(&'a LevelTilesAsset),
}
pub struct VoxelZoneBuilder<'a> {
    position: WorldCoord,
    source: VoxelZoneSource<'a>,
    entrances: Vec<ZoneEntrance>,
}

impl<'a> VoxelZoneBuilder<'a> {
    pub fn new(position: WorldCoord, source: VoxelZoneSource<'a>) -> Self {
        Self {
            position,
            source,
            entrances: Vec::new(),
        }
    }

    pub fn add_entrance(mut self, entrance_id: u32, entrance_location: ZoneCoord) -> Self {
        self.entrances
            .push(ZoneEntrance::unconnected(entrance_id, entrance_location));
        self
    }

    pub fn spawn(
        self,
        commands: &mut Commands,
        asset_server: &AssetServer,
        meshes: &mut Assets<Mesh>,
        materials: &mut Assets<StandardMaterial>,
    ) -> Entity {
        let zone_origin = self.position;
        let zone_voxels = match &self.source {
            VoxelZoneSource::SizedBox(size) => LevelVoxels::from_size(size.0),
            VoxelZoneSource::Level(level) => LevelVoxels::from_tiles(level),
        };
        let zone_size = zone_voxels.size;

        let desired_chunk_width = 32;
        let full_chunk_amounts = zone_size.0 / desired_chunk_width;
        let chunk_rests = zone_size.0.map(|s| s % desired_chunk_width);
        let total_chunk_amounts =
            full_chunk_amounts + chunk_rests.map(|v| if v > 0 { 1 } else { 0 });

        let get_chunk_width = |index: u32, chunk_rest: u32, total_chunk_amount: u32| {
            if chunk_rest != 0 && index + 1 == total_chunk_amount {
                chunk_rest
            } else {
                desired_chunk_width
            }
        };

        let entrances = self.entrances.clone();

        let voxel_texture_h = asset_server.load("voxels.png");

        // The entity for the zone itself
        let zone_entity = commands
            .spawn()
            .insert(Transform::from_xyz(
                zone_origin.x,
                zone_origin.y,
                zone_origin.z,
            ))
            .insert(GlobalTransform {
                ..Default::default()
            })
            .insert(zone::ZoneSize(zone_size))
            .insert(zone::ZoneEntrances {
                entrances: self.entrances,
            })
            .insert(zone::ZoneWorldBounds(bounds::Bounds3::zero()))
            .with_children(|parent| {
                for chunk_z in 0..total_chunk_amounts.z {
                    let chunk_width_z =
                        get_chunk_width(chunk_z, chunk_rests.z, total_chunk_amounts.z);
                    for chunk_y in 0..total_chunk_amounts.y {
                        let chunk_width_y =
                            get_chunk_width(chunk_y, chunk_rests.y, total_chunk_amounts.y);

                        for chunk_x in 0..total_chunk_amounts.x {
                            let chunk_width_x =
                                get_chunk_width(chunk_x, chunk_rests.x, total_chunk_amounts.x);

                            let zone_location = glm::vec3(chunk_x, chunk_y, chunk_z);
                            let position = zone_location.map(|v| (v * desired_chunk_width) as f32);

                            //println!(
                            //    "creating chunk {},{},{} with width {},{},{} at: {:?}",
                            //    chunk_x,
                            //    chunk_y,
                            //    chunk_z,
                            //    chunk_width_x,
                            //    chunk_width_y,
                            //    chunk_width_z,
                            //    position
                            //);

                            // Now spawn all child chunk entities
                            let chunk = level_chunk(
                                ZoneCoord::new(
                                    chunk_x * desired_chunk_width,
                                    chunk_y * desired_chunk_width,
                                    chunk_z * desired_chunk_width,
                                ),
                                ChunkCoord::new(chunk_width_x, chunk_width_y, chunk_width_z),
                                &zone_voxels,
                                &entrances,
                            );

                            if !chunk.is_empty() {
                                parent.spawn_bundle(PbrBundle {
                                    mesh: meshes.add(chunk.into()), // TODO: When are meshes cleaned up?

                                    material: materials.add(StandardMaterial {
                                        base_color_texture: Some(voxel_texture_h.clone()),
                                        unlit: false,
                                        ..Default::default()
                                    }), // TODO: Maybe don't create a new all the time
                                    transform: Transform::from_xyz(
                                        position.x, position.y, position.z,
                                    ),
                                    ..Default::default()
                                });
                            }
                        }
                    }
                }
            })
            .id();

        println!(
            "spawning voxel zone at {:?} with voxel size {:?}",
            zone_origin, zone_size
        );
        println!(
            "this results in {:?} chunks with rest chunks of sizes: {:?}",
            full_chunk_amounts, chunk_rests
        );
        zone_entity
    }
}

fn _random_chunk(size: ChunkCoord) -> voxels::Chunk {
    let mut rng = rand::thread_rng();

    let mut chunk = voxels::Chunk::new(size);

    for v in chunk.iter_voxels_mut() {
        let solid: bool = rng.gen();
        *v = if solid {
            Some(voxels::Type::Grass)
        } else {
            None
        };
    }
    chunk
}

fn room_chunk(
    chunk_pos: ZoneCoord,
    chunk_size: ChunkCoord,
    ZoneCoord(room_size): ZoneCoord,
    entrances: &[ZoneEntrance],
) -> voxels::Chunk {
    let mut rng = rand::thread_rng();
    let is_wall = |ZoneCoord(p)| {
        p.x == 0
            || p.y == 0
            || p.z == 0
            || p.x + 1 == room_size.x
            || p.y + 1 == room_size.y
            || p.z + 1 == room_size.z
    };
    let is_entrance = |ZoneCoord(p)| -> bool {
        entrances.iter().any(|e| {
            let entrance_size = ZoneEntrance::size();
            let entrance_start = e.location;
            let ZoneCoord(entrance_end) = entrance_start + entrance_size;
            let ZoneCoord(entrance_start) = entrance_start;

            p.x >= entrance_start.x
                && p.x < entrance_end.x
                && p.y >= entrance_start.y
                && p.y < entrance_end.y
                && p.z >= entrance_start.z
                && p.z < entrance_end.z
        })
    };

    let mut chunk = voxels::Chunk::new(chunk_size);

    for (coord, voxel) in chunk.iter_coords().zip(chunk.iter_voxels_mut()) {
        let absolute = chunk_pos + coord;
        let solid = is_wall(absolute) && !is_entrance(absolute);
        *voxel = if solid {
            Some(
                *[voxels::Type::Grass, voxels::Type::Road, voxels::Type::Moss]
                    .choose(&mut rng)
                    .unwrap(),
            )
        } else {
            None
        };
    }
    chunk
}

fn level_chunk(
    chunk_pos: ZoneCoord,
    chunk_size: ChunkCoord,
    level: &LevelVoxels,
    entrances: &[ZoneEntrance],
) -> voxels::Chunk {
    let mut chunk = voxels::Chunk::new(chunk_size);

    for (coord, voxel) in chunk.iter_coords().zip(chunk.iter_voxels_mut()) {
        let absolute = chunk_pos + coord;
        *voxel = level.get(&absolute.0);
    }
    chunk
}
