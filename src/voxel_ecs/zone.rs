use bevy::prelude::*;

use crate::{
    bounds::Bounds3,
    voxels::{self, WorldCoord, ZoneCoord, ZoneEntrance},
};

/// A Component for knowing how big a zone is in voxels
#[derive(Debug, Clone, Copy, Component)]
pub struct ZoneSize(pub voxels::ZoneCoord);

impl ZoneSize {
    pub fn to_world(self) -> WorldCoord {
        self.0.to_world(&glm::zero())
    }
}

/// A component that holds zone entrances for a zone
#[derive(Debug, Clone, Component)]
pub struct ZoneEntrances {
    pub entrances: Vec<ZoneEntrance>,
}

impl ZoneEntrances {
    pub fn find_by_id(&self, id: u32) -> Option<&ZoneEntrance> {
        self.entrances.iter().find(|e| e.id == id)
    }
}

/// A component that represents the world space span of a zone
#[derive(Debug, Copy, Clone, Component)]
pub struct ZoneWorldBounds(pub Bounds3<f32>);

impl ZoneWorldBounds {
    pub fn new(start: WorldCoord, size: WorldCoord) -> Self {
        Self(Bounds3::with_start_size(start, size))
    }
    pub fn contains(&self, pos: &WorldCoord) -> bool {
        self.0.contains(pos)
    }
}

/// A component that represents bounds of something inside a zone
#[derive(Debug, Copy, Clone)]
pub struct ZoneBounds(pub Bounds3<u32>);

impl ZoneBounds {
    pub fn new(start: ZoneCoord, size: ZoneCoord) -> Self {
        Self(Bounds3::with_start_size(start.0, size.0))
    }
    pub fn contains(&self, pos: &ZoneCoord) -> bool {
        self.0.contains(&pos.0)
    }
}

pub fn update_zone_spans(
    // TODO: make this happen only when needed, not every frame
    mut q: Query<(&Transform, &ZoneSize, &mut ZoneWorldBounds)>,
) {
    for (transform, size, mut span) in q.iter_mut() {
        let start = util::to_glm(transform.translation);
        let size = size.to_world();
        *span = ZoneWorldBounds::new(start, size);
    }
}

pub fn find_containing_zone<'a>(
    mut iter: impl Iterator<Item = (Entity, &'a ZoneWorldBounds)>,
    pos: &WorldCoord,
) -> Option<Entity> {
    iter.find_map(|(entity, bounds)| {
        if bounds.contains(pos) {
            Some(entity)
        } else {
            None
        }
    })
}
