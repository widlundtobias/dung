use bevy::{prelude::Mesh, render::render_resource::PrimitiveTopology};

use crate::voxels::{tex_coords, WorldCoord};

use super::{ChunkCoord, Direction, Type};

pub type ChunkCell = Option<Type>;
pub struct Chunk {
    voxels: Vec<ChunkCell>,
    size: ChunkCoord,
}

impl Chunk {
    pub fn new(size: ChunkCoord) -> Self {
        let voxel_count = size.0.x * size.0.y * size.0.z;
        Self {
            voxels: vec![Default::default(); voxel_count as usize],
            size,
        }
    }
    pub fn _set(&mut self, coord: &ChunkCoord, value: ChunkCell) {
        let index = self.coord_to_index(coord);

        // TODO: What should happen on error
        if let Some(v) = self.voxels.get_mut(index) {
            *v = value;
        }
    }

    pub fn get(&self, coord: &ChunkCoord) -> Option<ChunkCell> {
        // TODO: bounds checking
        let index = self.coord_to_index(coord);
        self.voxels.get(index).cloned()
    }

    pub fn iter_coords(&self) -> impl Iterator<Item = ChunkCoord> {
        ChunkCoordIterator::new(self.size)
    }
    pub fn iter_voxels(&self) -> impl Iterator<Item = &ChunkCell> {
        self.voxels.iter()
    }
    pub fn iter_voxels_mut(&mut self) -> impl Iterator<Item = &mut ChunkCell> {
        self.voxels.iter_mut()
    }
    pub fn voxel_count(&self) -> usize {
        self.voxels.len()
    }
    pub fn is_empty(&self) -> bool {
        self.voxels.iter().all(Option::is_none)
    }
}

impl Chunk {
    fn coord_to_index(&self, coord: &ChunkCoord) -> usize {
        let v = coord.0.map(|v| v as usize);
        let s = self.size.0.map(|v| v as usize);

        v.x + v.y * s.x + v.z * s.x * s.y
    }
}

pub struct ChunkCoordIterator {
    i: usize,
    x: u32,
    y: u32,
    z: u32,
    total: usize,
    size: ChunkCoord,
}

impl ChunkCoordIterator {
    pub fn new(size: ChunkCoord) -> Self {
        Self {
            i: 0,
            x: 0,
            y: 0,
            z: 0,
            total: (size.0.x * size.0.y * size.0.z) as usize,
            size,
        }
    }
}

impl Iterator for ChunkCoordIterator {
    type Item = ChunkCoord;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i == self.total {
            // Exhausted
            None
        } else {
            let coord = ChunkCoord::new(self.x, self.y, self.z);

            self.i += 1;

            let triggers_flip = self.size.0.xy().map(|v| v - 1);

            if self.x == triggers_flip.x {
                self.x = 0;
                if self.y == triggers_flip.y {
                    self.y = 0;
                    self.z += 1;
                } else {
                    self.y += 1;
                }
            } else {
                self.x += 1;
            }

            Some(coord)
        }
    }
}

impl From<Chunk> for Mesh {
    fn from(chunk: Chunk) -> Self {
        let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);

        let mut positions = Vec::new(); // TODO: with capacity
        let mut normals = Vec::new();
        let mut uvs = Vec::new();

        for (coord, voxel) in chunk.iter_coords().zip(chunk.iter_voxels()) {
            if let Some(voxel) = voxel {
                let directions = Direction::iter();
                let neighbors = directions.map(|d| (d, coord.neighbor(&chunk.size, d)));
                let empty_neighbors = neighbors.filter_map(|(d, coord)| {
                    let voxel = coord.and_then(|c| chunk.get(&c));
                    if voxel == Some(Some(Type::Grass)) {
                        None
                    } else {
                        Some(d)
                    }
                });

                let face_width = 1.0;

                let voxel_pos = glm::convert::<_, WorldCoord>(coord.0) * face_width;
                let tex_coords = tex_coords(*voxel);

                for face_to_generate in empty_neighbors {
                    let min = 0.0 * face_width;
                    let max = 1.0 * face_width;

                    let tmin = tex_coords[0];
                    let tmax = tex_coords[1];

                    let vertices = match face_to_generate {
                        Direction::Left => [
                            ([min, min, max], [-1.0, 0., 0.], [tmax.x, tmin.y]),
                            ([min, max, max], [-1.0, 0., 0.], [tmin.x, tmin.y]),
                            ([min, max, min], [-1.0, 0., 0.], [tmin.x, tmax.y]),
                            ([min, max, min], [-1.0, 0., 0.], [tmin.x, tmax.y]),
                            ([min, min, min], [-1.0, 0., 0.], [tmax.x, tmax.y]),
                            ([min, min, max], [-1.0, 0., 0.], [tmax.x, tmin.y]),
                        ],
                        Direction::Right => [
                            ([max, min, min], [1.0, 0., 0.], [tmin.x, tmin.y]),
                            ([max, max, min], [1.0, 0., 0.], [tmax.x, tmin.y]),
                            ([max, max, max], [1.0, 0., 0.], [tmax.x, tmax.y]),
                            ([max, max, max], [1.0, 0., 0.], [tmax.x, tmax.y]),
                            ([max, min, max], [1.0, 0., 0.], [tmin.x, tmax.y]),
                            ([max, min, min], [1.0, 0., 0.], [tmin.x, tmin.y]),
                        ],
                        Direction::Bottom => [
                            ([max, min, max], [0., -1.0, 0.], [tmin.x, tmin.y]),
                            ([min, min, max], [0., -1.0, 0.], [tmax.x, tmin.y]),
                            ([min, min, min], [0., -1.0, 0.], [tmax.x, tmax.y]),
                            ([min, min, min], [0., -1.0, 0.], [tmax.x, tmax.y]),
                            ([max, min, min], [0., -1.0, 0.], [tmin.x, tmax.y]),
                            ([max, min, max], [0., -1.0, 0.], [tmin.x, tmin.y]),
                        ],
                        Direction::Top => [
                            ([max, max, min], [0., 1.0, 0.], [tmax.x, tmin.y]),
                            ([min, max, min], [0., 1.0, 0.], [tmin.x, tmin.y]),
                            ([min, max, max], [0., 1.0, 0.], [tmin.x, tmax.y]),
                            ([min, max, max], [0., 1.0, 0.], [tmin.x, tmax.y]),
                            ([max, max, max], [0., 1.0, 0.], [tmax.x, tmax.y]),
                            ([max, max, min], [0., 1.0, 0.], [tmax.x, tmin.y]),
                        ],
                        Direction::Back => [
                            ([min, max, min], [0., 0., -1.0], [tmax.x, tmin.y]),
                            ([max, max, min], [0., 0., -1.0], [tmin.x, tmin.y]),
                            ([max, min, min], [0., 0., -1.0], [tmin.x, tmax.y]),
                            ([max, min, min], [0., 0., -1.0], [tmin.x, tmax.y]),
                            ([min, min, min], [0., 0., -1.0], [tmax.x, tmax.y]),
                            ([min, max, min], [0., 0., -1.0], [tmax.x, tmin.y]),
                        ],
                        Direction::Front => [
                            ([min, min, max], [0., 0., 1.0], [tmin.x, tmin.y]),
                            ([max, min, max], [0., 0., 1.0], [tmax.x, tmin.y]),
                            ([max, max, max], [0., 0., 1.0], [tmax.x, tmax.y]),
                            ([max, max, max], [0., 0., 1.0], [tmax.x, tmax.y]),
                            ([min, max, max], [0., 0., 1.0], [tmin.x, tmax.y]),
                            ([min, min, max], [0., 0., 1.0], [tmin.x, tmin.y]),
                        ],
                    };

                    for (position, normal, uv) in vertices.iter() {
                        let position = [
                            voxel_pos.x + position[0],
                            voxel_pos.y + position[1],
                            voxel_pos.z + position[2],
                        ];
                        positions.push(position);
                        normals.push(*normal);
                        uvs.push(*uv);
                    }
                }
            }
        }

        mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, positions);
        mesh.set_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
        mesh.set_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
        mesh
    }
}
