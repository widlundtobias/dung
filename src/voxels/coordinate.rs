use super::Direction;

/// World space coordinate
pub type WorldCoord = glm::Vec3;

/// Voxel coord inside a zone
#[derive(Debug, Clone, Copy)]
pub struct ZoneCoord(pub glm::UVec3);

impl ZoneCoord {
    pub fn new(x: u32, y: u32, z: u32) -> Self {
        Self(glm::vec3(x, y, z))
    }
    pub fn to_world(self, zone_origin: &WorldCoord) -> WorldCoord {
        zone_origin + glm::convert::<_, glm::Vec3>(self.0)
    }
    pub fn as_world_size(self) -> WorldCoord {
        glm::convert::<_, glm::Vec3>(self.0)
    }
}

impl std::ops::Add<ZoneCoord> for ZoneCoord {
    type Output = ZoneCoord;

    fn add(self, rhs: ZoneCoord) -> Self::Output {
        ZoneCoord(self.0 + rhs.0)
    }
}

impl std::ops::Add<ChunkCoord> for ZoneCoord {
    type Output = ZoneCoord;

    fn add(self, rhs: ChunkCoord) -> Self::Output {
        ZoneCoord(self.0 + rhs.0)
    }
}

/// Voxel coord inside a chunk
#[derive(Debug, Clone, Copy)]
pub struct ChunkCoord(pub glm::UVec3);

impl ChunkCoord {
    pub fn new(x: u32, y: u32, z: u32) -> Self {
        Self(glm::vec3(x, y, z))
    }
    pub fn neighbor(&self, chunk_size: &ChunkCoord, direction: Direction) -> Option<ChunkCoord> {
        let x = self.0.x;
        let y = self.0.y;
        let z = self.0.z;

        let last = chunk_size.0.map(|v| v - 1);

        match direction {
            Direction::Left if x > 0 => Some(ChunkCoord::new(x - 1, y, z)),
            Direction::Right if x < last.x => Some(ChunkCoord::new(x + 1, y, z)),
            Direction::Bottom if y > 0 => Some(ChunkCoord::new(x, y - 1, z)),
            Direction::Top if y < last.y => Some(ChunkCoord::new(x, y + 1, z)),
            Direction::Back if z > 0 => Some(ChunkCoord::new(x, y, z - 1)),
            Direction::Front if z < last.z => Some(ChunkCoord::new(x, y, z + 1)),
            _ => None,
        }
    }
}
