#[derive(Debug, Clone, Copy)]
pub enum Direction {
    Left,
    Right,
    Bottom,
    Top,
    Back,
    Front,
}

impl Direction {
    pub fn iter() -> impl Iterator<Item = Direction> {
        [
            Direction::Left,
            Direction::Right,
            Direction::Bottom,
            Direction::Top,
            Direction::Back,
            Direction::Front,
        ]
        .into_iter()
    }
}
