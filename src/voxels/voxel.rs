#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Type {
    Grass,
    Road,
    Moss,
    Sand,
    Planks,
    Granite,
    DryGrass,
    Water,
    Cactus,
}

pub fn tex_coords(t: Type) -> [glm::Vec2; 2] {
    let tex_cell_size = glm::vec2(16. / 256., 16. / 256.);

    let to_tex = |x: u32, y: u32| {
        let start = glm::vec2(x as f32, y as f32).component_mul(&tex_cell_size);
        let end = start + tex_cell_size;
        [start, end]
    };
    match t {
        Type::Grass => to_tex(0, 0),
        Type::Road => to_tex(1, 0),
        Type::Moss => to_tex(2, 0),
        Type::Sand => to_tex(3, 0),
        Type::Planks => to_tex(4, 0),
        Type::Granite => to_tex(5, 0),
        Type::DryGrass => to_tex(6, 0),
        Type::Water => to_tex(7, 0),
        Type::Cactus => to_tex(8, 0),
    }
}
