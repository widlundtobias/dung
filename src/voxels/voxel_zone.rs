use bevy::prelude::Entity;

use crate::bounds::{self};

use super::{WorldCoord, ZoneCoord};

/// A single zone entrance with location relative to the owning zone
#[derive(Debug, Clone, Copy)]
pub struct ZoneEntrance {
    pub id: u32,
    pub location: ZoneCoord,
    pub target: Option<ZoneEntranceRef>,
}

impl ZoneEntrance {
    pub fn unconnected(id: u32, location: ZoneCoord) -> Self {
        Self {
            id,
            location,
            target: None,
        }
    }
    pub fn world_bounds(&self, zone_origin: &WorldCoord) -> bounds::Bounds3<f32> {
        let start = self.location.to_world(zone_origin);
        let size = Self::size().as_world_size();

        bounds::Bounds3::with_start_size(start, size)
    }
    pub fn size() -> ZoneCoord {
        ZoneCoord::new(1, 2, 1)
    }
}

/// A reference to an entrance inside of a particular zone
#[derive(Debug, Clone, Copy)]
pub struct ZoneEntranceRef {
    pub zone: Entity,
    pub entrance_id: u32,
}
